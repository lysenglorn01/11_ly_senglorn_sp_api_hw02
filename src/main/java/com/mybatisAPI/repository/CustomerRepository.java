package com.mybatisAPI.repository;

import com.mybatisAPI.model.entity.Customer;
import com.mybatisAPI.model.request.CustomerRequest;
import com.mybatisAPI.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
@Mapper
public interface CustomerRepository  {
    //Query all data
    @Select("SELECT * FROM tb_customer")
    List<Customer> findAllCustomer();

    //Query by ID
    @Select ("SELECT * FROM tb_customer WHERE customer_id = #{customerId}")
    Customer getCustomerById(Integer customerId);

    //Add new Customer in database
    @Select("INSERT INTO tb_customer(customer_name, customer_address, customer_phone) " +
            "VALUES(#{request.customer_name}, #{request.customer_address}, #{request.customer_phone})" +
            "RETURNING *")
    Customer addNewCustomer (@Param("request") CustomerRequest productRequest);

    @Select("UPDATE tb_customer " +
            "SET customer_name = #{request.customer_name}, " +
            "customer_address = #{request.customer_address},  " +
            "customer_phone = #{request.customer_phone} " +
            "WHERE customer_id = #{customerId} " +
            "RETURNING *")
    Customer updateCustomer (@Param("request") CustomerRequest customerRequest, Integer customerId);



    //Delete record in database
    @Delete("DELETE FROM tb_customer WHERE customer_id = #{customerId}")
    boolean deleteCustomerById (Integer customerId);
}
