package com.mybatisAPI.repository;

import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    //Query all product
    @Select("SELECT * FROM tb_product")
    List<Product> findAllProduct();

    //Query product by id
    @Select ("SELECT * FROM tb_product WHERE product_id = #{productId}")
//    @ResultMap("productMap")
    Product getProductById(Integer productId);

    //Add new product in database
    @Select("INSERT INTO tb_product(product_name, product_price) " +
            "VALUES(#{request.product_name}, #{request.product_price})" +
            "RETURNING product_id")
    Integer addNewProduct (@Param("request") ProductRequest productRequest);

    //update record
    @Select("UPDATE tb_product " +
            "SET product_name = #{request.product_name}," +
            "product_price = #{request.product_price} " +
            "WHERE product_id = #{productId} " +
            "RETURNING product_id")
    Integer updateProduct (@Param("request") ProductRequest productRequest, Integer productId);

    //Delete product
    @Delete("DELETE FROM tb_product WHERE product_id = #{productId}")
    boolean deleteProductById (Integer productId);
}
