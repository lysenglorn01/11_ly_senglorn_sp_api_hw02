package com.mybatisAPI.repository;

import com.mybatisAPI.model.entity.Invoice;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM tb_invoice")
    @Results(id = "invoiceMap",value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                    one = @One(select = "com.mybatisAPI.repository.CustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "com.mybatisAPI.repository.ProductRepository.getProductById"))
    })
    List<Invoice> findAllInvoice();

    //get invoice by id
    @Select("SELECT * FROM tb_invoice WHERE invoice_id = #{invoiceId}")
    @ResultMap("invoiceMap")
    Invoice getInvoiceById(Integer invoiceId);
}
