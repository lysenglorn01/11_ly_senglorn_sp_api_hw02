package com.mybatisAPI.service.ServiceImp;

import com.mybatisAPI.model.entity.Invoice;
import com.mybatisAPI.model.response.InvoiceResponse;
import com.mybatisAPI.repository.InvoiceRepository;
import com.mybatisAPI.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }
    //get all invoice
    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Integer addNewInvoice(InvoiceResponse invoiceResponse) {
        return null;
    }

    //get invoice by id
    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }
}
