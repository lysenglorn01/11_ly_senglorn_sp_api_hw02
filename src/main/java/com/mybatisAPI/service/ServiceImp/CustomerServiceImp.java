package com.mybatisAPI.service.ServiceImp;

import com.mybatisAPI.model.entity.Customer;
import com.mybatisAPI.model.request.CustomerRequest;
import com.mybatisAPI.model.request.ProductRequest;
import com.mybatisAPI.repository.CustomerRepository;
import com.mybatisAPI.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //get call customer in database
    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    //get customer by id
    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    //add new customer
    @Override
    public Customer addNewCustomer(CustomerRequest customerRequest) {
        Customer customerId = customerRepository.addNewCustomer(customerRequest);
        return customerId;
    }

//    @Override
//    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
//        Integer productIdUpdate = productRepository.updateProduct(productRequest, productId);
//        return productIdUpdate;
//    }

    @Override
    public Customer updateCustomerById(CustomerRequest customerRequest, Integer customerId) {
        return customerRepository.updateCustomer(customerRequest,customerId);
    }


    //delete
    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }
}
