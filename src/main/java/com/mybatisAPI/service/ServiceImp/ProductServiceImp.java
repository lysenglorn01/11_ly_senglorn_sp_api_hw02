package com.mybatisAPI.service.ServiceImp;

import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.request.ProductRequest;
import com.mybatisAPI.repository.ProductRepository;
import com.mybatisAPI.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    //Get all product
    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    //Get Product By ID
    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    // add new
    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId = productRepository.addNewProduct(productRequest);

        return  productId;
    }

    //Update product
    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdUpdate = productRepository.updateProduct(productRequest, productId);
        return productIdUpdate;
    }

    //Delete
    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }
}
