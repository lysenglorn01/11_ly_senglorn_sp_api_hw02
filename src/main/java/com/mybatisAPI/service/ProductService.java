package com.mybatisAPI.service;

import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    //Get all product
    List<Product> getAllProduct();

    //Get by id
    Product getProductById (Integer productId);

    //Add product
    Integer addNewProduct (ProductRequest productRequest);

    //Update
    Integer updateProduct (ProductRequest productRequest, Integer productId);

    //Delete product
    boolean deleteProductById (Integer productId);
}
