package com.mybatisAPI.service;

import com.mybatisAPI.model.entity.Customer;
import com.mybatisAPI.model.request.CustomerRequest;
import com.mybatisAPI.model.response.CustomerResponse;

import java.util.List;

public interface CustomerService {
    //Get Customer
    List<Customer> getAllCustomer();

    //Get Customer by id
    Customer getCustomerById (Integer customerId);

    //Add Customer
    Customer addNewCustomer (CustomerRequest customerRequest);

    //Update Customer
    Customer updateCustomerById (CustomerRequest customerRequest, Integer customerId);

    //Delete Customer
    boolean deleteCustomerById (Integer customerId);
}
