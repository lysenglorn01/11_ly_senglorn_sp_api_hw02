package com.mybatisAPI.service;

import com.mybatisAPI.model.entity.Invoice;
import com.mybatisAPI.model.response.InvoiceResponse;

import java.util.List;

public interface InvoiceService {
    //get all Invoice
    List<Invoice> getAllInvoice();

    //get invoice by id
    Invoice getInvoiceById(Integer invoiceId);

    //Add New invoice
    Integer addNewInvoice(InvoiceResponse invoiceResponse);
}
