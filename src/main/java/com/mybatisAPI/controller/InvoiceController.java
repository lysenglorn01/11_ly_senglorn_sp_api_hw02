package com.mybatisAPI.controller;

import com.mybatisAPI.model.entity.Invoice;
import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.response.InvoiceResponse;
import com.mybatisAPI.model.response.ProductResponse;
import com.mybatisAPI.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.util.List;

@Controller
@RequestMapping("api/vi1/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all")
    @Operation(summary = "Get all invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice() {
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .invoice(invoiceService.getAllInvoice())
                .message("You are fetch all invoice successfully")
                .httpStatus(HttpStatus.OK)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-byId/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        if(invoiceService.getInvoiceById(invoiceId) != null){
            response = InvoiceResponse.<Invoice>builder()
                    .invoice(invoiceService.getInvoiceById(invoiceId))
                    .message("Fetch all invoice successfully")
                    .httpStatus(HttpStatus.OK)
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            return ResponseEntity.badRequest().body(response);
        }

    }

}
