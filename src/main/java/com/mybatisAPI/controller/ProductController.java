package com.mybatisAPI.controller;

import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.request.ProductRequest;
import com.mybatisAPI.model.response.ProductResponse;
import com.mybatisAPI.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.Builder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("api/vi/products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    //Fetch all recorde product
    @GetMapping("/get-all")
    @Operation (summary = "Get all products")
    public ResponseEntity <ProductResponse<List<Product>>>  getAllProduct() {
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .product(productService.getAllProduct())
                .message("You are fetch all data successfully")
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    //Get Product By ID
    @GetMapping("/get-byId/{id}")
    @Operation (summary = "Get product by id")
    public ResponseEntity <ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId) {
        ProductResponse<Product> response = null;
        if (productService.getProductById(productId) != null){
                 response = ProductResponse.<Product>builder()
                        .message("Your Record by id is successfully found!")
                        .product(productService.getProductById(productId))
                        .httpStatus(HttpStatus.OK)
                        .timestamp(new Timestamp(System.currentTimeMillis()))
                        .build();
                return ResponseEntity.ok(response);
            }
        else  {
            response = ProductResponse.<Product>builder()
                    .message("Your Data not found!")
                    .product(productService.getProductById(productId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    //Add New product
    @PostMapping("/addNew")
    @Operation (summary = "Add new products")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest) {
        Integer productStoreId = productService.addNewProduct(productRequest);

        if (productStoreId != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Add New Product Success")
                    .product(productService.getProductById(productStoreId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    //Update product
    @PutMapping("/update/{id}")
    @Operation (summary = "Update record product")
    public ResponseEntity<ProductResponse<Product>> updateNewProduct(@RequestBody ProductRequest productRequest,
                                                                     @PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        Integer productIdUpdate = productService.updateProduct(productRequest, productId);
        if (productIdUpdate != null){
            response = ProductResponse.<Product>builder()
                    .message("Your Product was update successfully")
                    .product(productService.getProductById(productIdUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = ProductResponse.<Product>builder()
                    .message("Don't have this product ID")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    //Delete Product by ID
    @DeleteMapping("/delete/{id}")
    @Operation (summary = "Delete products by ID")
    public ResponseEntity<ProductResponse <String>> deleteProductById(@PathVariable("id") Integer productId) {
        ProductResponse<String> response = null;
        boolean productIdDelete = productService.deleteProductById(productId);
        if (productIdDelete == true) {
            response = ProductResponse.<String>builder()
                    .message("Your product was delete successfully!")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        else {
            response = ProductResponse.<String>builder()
                    .message("Dont have this product ID")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
            return ResponseEntity.ok(response);
    }

}
