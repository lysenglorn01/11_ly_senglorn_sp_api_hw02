package com.mybatisAPI.controller;

import com.mybatisAPI.model.entity.Customer;
import com.mybatisAPI.model.entity.Product;
import com.mybatisAPI.model.request.CustomerRequest;
import com.mybatisAPI.model.request.ProductRequest;
import com.mybatisAPI.model.response.CustomerResponse;
import com.mybatisAPI.model.response.ProductResponse;
import com.mybatisAPI.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    //Fetch all recorde product
    @GetMapping("/get-all")
    @Operation(summary = "Get all customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllProduct() {
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .customers(customerService.getAllCustomer())
                .message("You are fetch all data customer successfully")
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    //Get Product By ID
    @GetMapping("/get-byId/{id}")
    @Operation (summary = "Get customer by id")
    public ResponseEntity <CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId) {
        CustomerResponse<Customer> response = null;
        if (customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Your customer Record by id is successfully found!")
                    .customers(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else  {
            response = CustomerResponse.<Customer>builder()
                    .message("Your Data not found!")
                    .customers(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    //Add New customer
    @PostMapping("/addNew")
    @Operation (summary = "Add new Customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
         Customer customerStoreId = customerService.addNewCustomer(customerRequest);

        if (customerStoreId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Add New customer was success!")
                    .customers(customerStoreId)
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    //Update customer
    @PutMapping("/update/{id}")
    @Operation (summary = "Update record customer")
    public ResponseEntity<CustomerResponse<Customer>> updateNewCustomer(@RequestBody CustomerRequest customerRequest,
                                                                     @PathVariable("id") Integer customerId){
        CustomerResponse<Customer> response = null;
        Customer customerIdUpdate = customerService.updateCustomerById(customerRequest, customerId);
        if (customerIdUpdate != null){
            response = CustomerResponse.<Customer>builder()
                    .message("Your customer was update successfully")
                    .customers(customerService.updateCustomerById(customerRequest,customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        else {
            response = CustomerResponse.<Customer>builder()
                    .message("Don't have this customer ID")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }


    @DeleteMapping("/delete/{id}")
    @Operation (summary = "Delete Customers")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId) {
        CustomerResponse<String> response = null;
        boolean CustomerIdDelete = customerService.deleteCustomerById(customerId);
        if (CustomerIdDelete == true) {
                    response = CustomerResponse.<String>builder()
                    .message("Your Delete was Successfully!")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        if (CustomerIdDelete != true) {
            response = CustomerResponse.<String>builder()
                    .message("Your not Find your customer")
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
        }
        return ResponseEntity.ok(response);
    }
}
