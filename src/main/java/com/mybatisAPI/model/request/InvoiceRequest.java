package com.mybatisAPI.model.request;

import java.sql.Timestamp;
import java.util.List;

public class InvoiceRequest {
    private Timestamp invoiceDate;
    private Integer customerId;
    private List<Integer> productId;
}
